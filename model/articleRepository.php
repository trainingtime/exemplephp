<?php  


function getArticles()
{
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM article  WHERE 1') ;

    $response->execute(array());

    return $response;
}


function getArticle($articleId)
{
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM article  WHERE id =:articleId') ;

    $response->execute(array('articleId' => $articleId));

    return $response;
}



function createArticle($authorId ,$title ,$body)

{
    $bdd = dbConnect();
    $response = $bdd->prepare ('INSERT INTO `blogue`.article(`authorId`, `title`, `body`)
            VALUES ( :authorId, :title, :body)');

    $response->execute(array('authorId' => $authorId,'title' => $title, 'body' => $body));

}

function updateArticle ($articleId, $title, $body)
{

    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `article` SET 
                                `title`= :title ,
                                `body`= :body
                                WHERE id = :articleId') ;

    $response->execute(array(   'articleId' => $articleId,
                                'title' => $title,
                                'body' => $body,
                                
    ));

}

function deleteArticle($articleId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `article` WHERE id= :articleId') ;

    $response = $response->execute(array(   'articleId' => $articleId));

}







    	function dbConnect(){
    try
    {
        return new PDO('mysql:host=localhost;dbname=blogue;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

