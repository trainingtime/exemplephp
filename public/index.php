<?php  

$uri = $_SERVER['REQUEST_URI'];


if($result = match($uri, "/article/create")){
    require("../Controller/article/createArticle.php");
    die;
}



if($result = match($uri, "/articles")){
    require("../Controller/article/displayArticles.php");
    die;
}

if($result = match($uri, "/article/:id")){
    require("../Controller/article/displayArticle.php");
    die;
}

if($result = match($uri, "/article/update/:id")){
    require("../Controller/article/updateArticle.php");
    die;
}

if($result = match($uri, "/article/delete/:id")){
    require("../Controller/article/deleteArticle.php");
    die;
}









if($result = match($uri, "/user/update/:id")){
    require("../Controller/user/updateUser.php");
    die;
}

if($result = match($uri, "/user/delete/:id")){
    require("../Controller/user/deleteUser.php");
    die;
}



if($result = match($uri, "/users")){
    require("../Controller/user/displayUsers.php");
    die;
}

  
if($result = match($uri, "/user/create")){
    require("../Controller/user/createUser.php");
    die;
}


if($result = match($uri, "/user/:id")){
    require("../Controller/user/displayUser.php");
    die;
}


























function match($url, $route){
   $path = preg_replace('#:([\w]+)#', '([^/]+)', $route); #finction match retourne vrai si la route et bonne
   $regex = "#^$path$#i";
   if(!preg_match($regex, $url, $matches)){
       return false;
   }
   return true;
}
