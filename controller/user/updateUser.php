<?php  

/*require("../Model/userRepository.php");




$authorId = getUserIdFromURI();
updateUser($authorId ,"fqd@faa.faa" ,"pass");






function getUserIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}
*/


$errors = new ArrayObject();
$authorId = getArticleIdFromURI();

require("../Model/userRepository.php");

if(!$response = userExist($authorId)){
	
    echo "This user does not exist";
    exit();
}




if(isFormValid($errors)){

    $authorId = getArticleIdFromURI();
    $email = $_POST['email'];
    $password = $_POST['password'];

    $bdd = dbConnect();

    $response = updateUser ($authorId, $email, $password);


    $response->closeCursor();
 

//    var_dump($_SERVER);
    die;
}



displayErrors($errors);
require("../view/user/updateuserView.php");




function getArticleIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}

function userExist($authorId)
{
	

    return getUser($authorId)->fetch();


}

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $authorId = getArticleIdFromURI();
    $email = $_POST['email'];
    $password = $_POST['password'];
    if(!fieldsArefilled($email, $password)){
        $errors->append('All the fields must be filled');
        return false;
    }

    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['email']) AND isset($_POST['password'])){
        return true;
    }
    return false;
}

function fieldsArefilled( $email, $password){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($email) OR empty($password)){
        return false; 
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}

