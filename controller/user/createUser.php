<?php
session_start();
$title="create User";
$errors = new ArrayObject();
if(isFormValid($errors)){

    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];

    require("../Model/userRepository.php");
    $bdd = dbConnect();
    $response = createUser($bdd, $email, $password);
    $lastInsertedId = $bdd->lastInsertId();

//    var_dump($lastInsertedId);
    if($lastInsertedId>0){
        header("location:../user/" . $lastInsertedId);die;
    }
    $errors->append("An error occurred, please contact your administrator system!");
}


displayErrors($errors);

require("../view/user/createUserview.php");



function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];

    if(!fieldsArefilled($email, $password, $confirm_password)){
        $errors->append('All the fields must be filled');
        return false;
    }

    if($password != $confirm_password){
        $errors->append('confirmation password error');
        return false;
    }

    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['email']) AND isset($_POST['password']) AND isset($_POST['confirm_password'])){
        return true;
    }

    return false;
}

function fieldsArefilled($email, $password, $confirm_password){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($email) OR empty($password) OR empty($confirm_password)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}




